package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorCommentMessages = new ArrayList<>();
		Comment comment = getComment(request);

		if (!isValid(comment, errorCommentMessages)) {
			request.getSession().setAttribute("comment", comment);
			request.getSession().setAttribute("errorCommentMessages", errorCommentMessages);
			response.sendRedirect("./");
			return;
		}

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());

		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private Comment getComment(HttpServletRequest request)
		throws IOException, ServletException {

		Comment comment = new Comment();
		comment.setText(request.getParameter("comment"));
		comment.setMessageId(Integer.parseInt(request.getParameter("user-message-id")));

		return comment;
	}

	private boolean isValid(Comment comment, List<String> errorCommentMessages) {

		String text = comment.getText();

		if (StringUtils.isBlank(text)) {
			errorCommentMessages.add("コメントを入力してください");
		} else if (text.length() > 500) {
			errorCommentMessages.add("コメントは500字以内で入力してください");
		}

		if (errorCommentMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
