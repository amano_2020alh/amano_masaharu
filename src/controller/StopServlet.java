package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		int isStopped = Integer.parseInt(request.getParameter("is-stopped"));
		int id = Integer.parseInt(request.getParameter("id"));

		new UserService().changeIsStopped(id, isStopped);

		response.sendRedirect("./management");
	}
}
