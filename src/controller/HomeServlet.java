package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		String startDateString = request.getParameter("start-date");
		String endDateString = request.getParameter("end-date");
		String category = request.getParameter("category");

		List<UserMessage> userMessages = new MessageService().select(startDateString, endDateString, category);
		List<UserComment> userComments = new CommentService().select();

		request.setAttribute("startDate", startDateString);
		request.setAttribute("endDate", endDateString);
		request.setAttribute("category", category);
		request.setAttribute("userMessages", userMessages);
		request.setAttribute("userComments", userComments);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
