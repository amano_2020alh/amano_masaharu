package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns= {"/login"})
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		List<String> errorMessages = new ArrayList<String>();

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントが入力されていません");
		}
		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません");
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		User user = new UserService().select(account, password);

		if (user == null) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		} else if (user.getIsStopped() == 1) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		request.getSession().setAttribute("loginUser", user);
		response.sendRedirect("./");
	}
}
