package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns= { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<>();

		// 支社情報の取得
		List<Branch> branches = new BranchService().select();
		request.setAttribute("branches", branches);

		// 部署情報の取得
		List<Department> departments = new DepartmentService().select();
		request.setAttribute("departments", departments);

		if (isInvalidQuery(request, errorMessages)) {
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		int userId = Integer.parseInt(request.getParameter("id"));
		User user = new UserService().select(userId);
		request.setAttribute("user", user);

		request.getRequestDispatcher("/setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<>();

		User user = getUser(request);

		if (!isValid(user, errorMessages)) {
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);

			// 支社情報の取得
			List<Branch> branches = new BranchService().select();
			request.setAttribute("branches", branches);

			// 部署情報の取得
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("departments", departments);

			request.getRequestDispatcher("/setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request)
		throws IOException, ServletException {

		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPasswordToConfirm(request.getParameter("password-to-confirm"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch-id")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department-id")));

		return user;
	}

	private boolean isValid(User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String passwordToConfirm = user.getPasswordToConfirm();
		int id = user.getId();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		} else if (name.length() > 10) {
			errorMessages.add("名前は10文字以内で入力してください");
		}

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (!Pattern.matches("[a-zA-Z0-9]{6,20}", account)) {
			errorMessages.add("アカウントは6文字以上20文字以下の半角英数字で入力してください");
		}

		if (!StringUtils.isEmpty(password) && !Pattern.matches("[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}", password)) {
			errorMessages.add("パスワードは6文字以上20文字以下の半角英数字/記号で入力してください");
		}

		if (!StringUtils.isEmpty(password) && !password.equals(passwordToConfirm)) {
			errorMessages.add("パスワードと確認用パスワードが一致しません");
		}

		User searchedUser = new UserService().select(account);

		if (searchedUser != null && id != searchedUser.getId()) {
			errorMessages.add("そのアカウント名は既に使用されているため使えません");
		}

		if (!((branchId == 1 && (departmentId == 1 || departmentId == 2)) ||
				(branchId == 2 && (departmentId == 3 || departmentId == 4)) ||
				(branchId == 3 && (departmentId == 3 || departmentId == 4)) ||
				(branchId == 4 && (departmentId == 3 || departmentId == 4)))) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private boolean isInvalidQuery(HttpServletRequest request, List<String> errorMessages) {

		String entryId = request.getParameter("id");
		boolean isInvalidNumber = false;

		if (StringUtils.isBlank(entryId)) {
			isInvalidNumber = true;
		}

		if (entryId.matches("\\d+")) {
			User user = new UserService().select(Integer.parseInt(entryId));
			if (user == null) {
				isInvalidNumber = true;
			}
		} else {
			isInvalidNumber = true;
		}

		if (isInvalidNumber) {
			errorMessages.add("不正なパラメータが入力されました");
		}

		return isInvalidNumber;
	}
}
