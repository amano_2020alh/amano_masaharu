package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<>();
		Message message = getMessage(request);

		if (!isValid(message, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("/message.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request)
		throws IOException, ServletException {

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));

		return message;
	}


	private boolean isValid(Message message, List<String> errorMessages) {
		String title = message.getTitle();
		String category = message.getCategory();
		String text = message.getText();

		validStringTest(title, "タイトル", 30, errorMessages);
		validStringTest(category, "カテゴリー", 10, errorMessages);
		validStringTest(text, "本文", 1000, errorMessages);

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private void validStringTest(String parameter, String parameterName, int maxLength, List<String> errorMessages) {

		if (StringUtils.isBlank(parameter)) {
			errorMessages.add(parameterName + "を入力してください");
		} else if (parameter.length() > maxLength) {
			errorMessages.add(parameterName + "は" + maxLength + "字以内で入力してください");
		}
	}
}
