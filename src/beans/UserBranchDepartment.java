package beans;

import java.io.Serializable;

public class UserBranchDepartment implements Serializable {

	private int id;
	private int isStopped;
	private String userName;
	private String account;
	private String branchName;
	private String departmentName;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
}
