package service;

import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String startDateString, String endDateString, String category) {

		final int LIMIT_NUM = 1000;

		Connection connection = null;

		try {
			connection = getConnection();

			String startDateTimeString = null;
			String endDateTimeString = null;

			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

			String defaultStartDate = "2020-01-01 00:00:00";
			String defaultEndDate = dateFormat.format(new Date());

			String startTime = " 00:00:00";
			String endTime = " 23:59:59";

			if (StringUtils.isEmpty(startDateString)) {
				startDateTimeString = defaultStartDate;
			} else {
				startDateTimeString = startDateString + startTime;
			}

			if (StringUtils.isEmpty(endDateString)) {
				endDateTimeString = defaultEndDate;
			} else {
				endDateTimeString = endDateString + endTime;
			}

			List<UserMessage> userMessages = new UserMessageDao().select(connection, startDateTimeString, endDateTimeString, category, LIMIT_NUM);
			commit(connection);

			return userMessages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int userMessageId) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().delete(connection, userMessageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}