package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({ "/management", "/setting", "/signup" })
public class ManagementFilter implements Filter {

	public final String INVALID_URL = "/setting";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		List<String> errorMessages = new ArrayList<>();

		if (!isManager(httpRequest, errorMessages)) {
			httpRequest.getSession().setAttribute("errorMessages", errorMessages);
			httpResponse.sendRedirect("./");
			return;
		} else {
			chain.doFilter(request, response); // サーブレットを実行
			return;
		}
	}

	private boolean isManager(HttpServletRequest httpRequest, List<String> errorManagementMessages) {

		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");

		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (branchId == 1 & departmentId == 1) {
			return true;
		} else {
			errorManagementMessages.add("権限がありません");
			return false;
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
