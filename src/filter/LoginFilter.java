package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	public static String URL_NOT_TO_FILTER = "/login";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		String currentPath = httpRequest.getServletPath();

		List<String> errorLoginMessages = new ArrayList<>();

		if (!currentPath.equals(URL_NOT_TO_FILTER) && !isLogined(httpRequest, errorLoginMessages)) {
			httpRequest.getSession().setAttribute("errorLoginMessages", errorLoginMessages);
			httpResponse.sendRedirect("./login");
			return;
		}

		chain.doFilter(request, response); // サーブレットを実行
		return;
	}

	private boolean isLogined(HttpServletRequest httpRequest, List<String> errorLoginMessages) {

		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");

		if (user == null) {
			errorLoginMessages.add("ログインしてください");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
