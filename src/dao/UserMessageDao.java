package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, String startDate, String endDate, String category, int limitNum) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT");
			sql.append("  users.name as user_name,");
			sql.append("  messages.title as title,");
			sql.append("  messages.category as category,");
			sql.append("  messages.text as text,");
			sql.append("  messages.created_date as created_date,");
			sql.append("  messages.id as id,");
			sql.append("  messages.user_id as user_id");
			sql.append(" FROM messages");
			sql.append(" INNER JOIN users");
			sql.append(" ON messages.user_id = users.id");
			sql.append(" WHERE");
			sql.append("  messages.created_date >= ?");
			sql.append(" AND");
			sql.append("  messages.created_date <= ?");

			boolean isCategoryBlank = StringUtils.isBlank(category);
			if (!isCategoryBlank) {
				sql.append(" AND");
				sql.append("  messages.category REGEXP ?");
			}

			sql.append(" ORDER BY created_date DESC");
			sql.append(" LIMIT " + limitNum + ";");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, startDate);
			ps.setString(2, endDate);

			if (!isCategoryBlank) {
				ps.setString(3, category);
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> userMessages = toUserMessages(rs);

			return userMessages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs)
		throws SQLException {

		List<UserMessage> userMessages = new ArrayList<>();

		try {
			while (rs.next()) {
				UserMessage userMessage = new UserMessage();
				userMessage.setUserName(rs.getString("user_name"));
				userMessage.setTitle(rs.getString("title"));
				userMessage.setCategory(rs.getString("category"));
				userMessage.setText(rs.getString("text"));
				userMessage.setCreatedDate(rs.getTimestamp("created_date"));
				userMessage.setId(rs.getInt("id"));
				userMessage.setUserId(rs.getInt("user_id"));

				userMessages.add(userMessage);
			}
			return userMessages;
		} finally {
			close(rs);
		}
	}
}
