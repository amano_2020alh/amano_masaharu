package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO messages (");
			sql.append("  user_id,");
			sql.append("  title,");
			sql.append("  category,");
			sql.append("  text,");
			sql.append("  created_date,");
			sql.append("  updated_date");
			sql.append(") VALUES (");
			sql.append("  ?,");
			sql.append("  ?,");
			sql.append("  ?,");
			sql.append("  ?,");
			sql.append("  CURRENT_TIMESTAMP,");
			sql.append("  CURRENT_TIMESTAMP");
			sql.append(");");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getTitle());
			ps.setString(3, message.getCategory());
			ps.setString(4, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int userMessageIdToDeletem) {

		PreparedStatement ps = null;

		try {
			// 投稿を削除し、紐づけられたコメントを削除する。
			StringBuilder sql = new StringBuilder();

			sql.append("DELETE");
			sql.append("  messages,");
			sql.append("  comments");
			sql.append(" FROM messages");
			sql.append(" LEFT JOIN comments");
			sql.append(" ON messages.id = comments.message_id");
			sql.append(" WHERE");
			sql.append("  messages.id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userMessageIdToDeletem);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}