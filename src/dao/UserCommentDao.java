package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection, int limitNum){

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT");
			sql.append("  comments.id as id,");    // コメントのid
			sql.append("  users.name as user_name,");    // コメントを行ったアカウントの名前
			sql.append("  comments.user_id as user_id,");    // コメントを行ったアカウントのid
			sql.append("  comments.text as text,");    // コメントの本文
			sql.append("  comments.created_date as created_date,");    // コメントの作成日時
			sql.append("  comments.message_id as message_id");    // コメントを付与するメッセージのid
			sql.append(" FROM comments");
			sql.append(" INNER JOIN users");
			sql.append(" ON comments.user_id = users.id");
			sql.append(" ORDER BY created_date ASC");
			sql.append(" LIMIT " + limitNum + ";");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserComments(rs);

			return userComments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs)
		throws SQLException {

		List<UserComment> userComments = new ArrayList<>();

		try {
			while (rs.next()) {
				UserComment userComment = new UserComment();
				userComment.setId(rs.getInt("id"));
				userComment.setUserName(rs.getString("user_name"));
				userComment.setUserId(rs.getInt("user_id"));
				userComment.setText(rs.getString("text"));
				userComment.setCreatedDate(rs.getTimestamp("created_date"));
				userComment.setMessageId(rs.getInt("message_id"));

				userComments.add(userComment);
			}
			return userComments;
		} finally {
			close(rs);
		}
	}

}
