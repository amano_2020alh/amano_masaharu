<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<title>掲示板</title>

		<script type="text/javascript">
			function deleteConfirm() {
				if (confirm("本当に削除しますか？")) {
					alert("削除しました");
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>

	<body>
		<div class="main-contents">

			<div class="header">
					<a href="message">新規投稿</a>
				<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
					<a href="management">ユーザー管理</a>
				</c:if>
					<a href="logout">ログアウト</a>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="./" id="search">
				<p>日付 <input name="start-date" type="date" value=${ startDate }> ～ <input name="end-date" type="date" value="${ endDate }"></p>
				<p>カテゴリ <input name="category" class="entry-box" value=<c:out value="${ category }"/>></p>
				<input type="submit" value="絞り込み" id="search-execute">
			</form>

			<div class="user-messages">
				<c:forEach items="${ userMessages }" var="userMessage">
					<div class="user-message">
						<div class="user-name">投稿者  <c:out value="${ userMessage.userName }" /></div>
						<div class="title">タイトル  <c:out value="${ userMessage.title }" /></div>
						<div class="category">カテゴリ  <c:out value="${ userMessage.category }" /></div>
						<div class="text" style="white-space: pre-wrap">本文  <c:out value="${ userMessage.text }" /></div>
						<div class="created-date">投稿日時  <fmt:formatDate value="${ userMessage.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>

						<c:if test="${ userMessage.userId == loginUser.id }">
							<form action="deleteMessage" method="post" onclick="return deleteConfirm()">
								<input type="hidden" name="user-message-id" value="${ userMessage.id }" >
								<input type="submit" value="削除">
							</form>
						</c:if>
					</div>

					<div class="form-area">

						<c:forEach items="${ userComments }" var="userComment">
							<c:if test="${ userMessage.id == userComment.messageId }" >
								<div class="user-comment">
									<div class="comment">投稿者  <c:out value="${ userComment.userName }" /></div>
									<div class="comment" >コメント  <c:out value="${ userComment.text }" /></div>
									<div class="comment">投稿日時  <fmt:formatDate value="${ userComment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></div>

									<c:if test="${ userComment.userId == loginUser.id }" >
										<form action="deleteComment" method="post" onclick="return deleteConfirm()">
											<input type="hidden" name="user-comment-id" value="${ userComment.id }">
											<input type="submit" value="削除">
										</form>
									</c:if>
								</div>
							</c:if>
						</c:forEach>

						<c:if test="${ userMessage.id == comment.messageId }">
							<c:if test="${ not empty errorCommentMessages }">
								<div class="error-comment-messages">
									<ul>
										<c:forEach items="${ errorCommentMessages }" var="errorCommentMessage">
											<li><c:out value="${ errorCommentMessage }" /></li>
										</c:forEach>
									</ul>
								</div>
								<c:remove var="errorCommentMessages" />
							</c:if>
						</c:if>

						<div class="post-comment">
							<form action="comment" method="post">コメント投稿
								<textarea name="comment" cols="40" rows="3" class="entry-box"><c:if test="${ userMessage.id == comment.messageId }"><c:out value="${ comment.text }"/></c:if></textarea>(500文字以内)
								<input type="hidden" name="user-message-id" value="${ userMessage.id }">
								<input type="submit" value="コメントする" >
							</form>
						</div>
					</div>

				</c:forEach>
				<c:remove var="comment" scope="session" />
			</div>

			<div class="copyright">Copyright(c) M.Amano</div>

		</div>
	</body>
</html>
