<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<title>ユーザー編集画面</title>
	</head>
	<body>
		<div class="main-contents">

			<div class="header">
				<a href="management">ユーザー管理</a>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" />
			</c:if>

			<div class="user-info">
				<form action="setting" method="post">
					<label for="account">アカウント名</label>
					<input name="account" id="account" value=<c:out value="${ user.account }" />>(半角英数字6文字以上20文字以下)

					<label for="password">パスワード</label>(記号を含む半角文字6文字以上20文字以下)
					<input name="password" type="password" id="password" >

					<label for="password-to-confirm">パスワード(確認用)</label>
					<input name="password-to-confirm" type="password" id="password-to-confirm" >

					<label for="name">名称</label>
					<input name="name" id="name" value=<c:out value="${ user.name }" />>(10文字以下)

					<c:choose>
						<c:when test="${ user.id != loginUser.id }">
							<label for="branch-id">支社</label>
								<select name="branch-id" id="branch-id">
									<c:forEach items="${ branches }" var="branch">
										<option value="${ branch.id }" <c:if test="${ branch.id == user.branchId }">selected</c:if>>
											<c:out value="${ branch.name }" />
										</option>
									</c:forEach>
								</select>

							<label for="department-id">部署</label>
								<select name="department-id">
									<c:forEach items="${ departments }" var="department">
										<option value="${ department.id }" <c:if test="${ department.id == user.departmentId }">selected</c:if>>
											<c:out value="${ department.name }" />
										</option>
									</c:forEach>
								</select>
						</c:when>

						<c:otherwise>
							<c:forEach items="${ branches }" var="branch">
								<c:if test="${ branch.id == user.branchId }">
									支社  <c:out value="${ branch.name }" />
									<input name="branch-id" id="branch-id" value="${ branch.id }" type="hidden" />
								</c:if>
							</c:forEach>

							<c:forEach items="${ departments }" var="department">
								<c:if test="${ department.id == user.departmentId }">
									部署  <c:out value="${ department.name }" />
									<input name="department-id" id="department-id" value="${ department.id }" type="hidden" />
								</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>

					<input type="hidden" name="id" id="id" value="${ user.id }">

					<input type="submit" value="更新">
				</form>
			</div>

			<div class="copyright">Copyright(c) M.Amano</div>

		</div>
	</body>
</html>