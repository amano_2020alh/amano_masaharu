<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<title>新規投稿画面</title>
	</head>

	<body>
		<div class="main-contents">

			<div class="header">
				<a href="./">ホーム</a>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" />
			</c:if>

			<div class="form-area">
		        <form action="message" method="post">
					<p>件名<input name="title" class="entry-box" value="${ message.title }" >(30文字以内)</p>
					<p>カテゴリ<input name="category" class="entry-box" value="${ message.category }">(10文字以内)</p>
					<p>本文<textarea name="text" cols="40" rows="5" class="entry-box"><c:out value="${ message.text }" /></textarea>(1000文字以内)</p>
		            <input type="submit" value="投稿">
		        </form>
			</div>

			<div class="copyright">Copyright(c) M.Amano</div>

		</div>
	</body>
</html>
