<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<title>ユーザー管理画面</title>

		<script type="text/javascript">
			function stopConfirm() {
				if (confirm("アカウントを停止しますか？")) {
					alert("停止しました");
					return true;
				} else {
					return false;
				}
			}
			function reviveConfirm() {
				if (confirm("アカウントを復活させますか？")) {
					alert("復活させました");
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>

	<body>
		<div class="main-contents">

			<div class="header">
				<a href="./">ホーム</a>
				<a href="signup">ユーザー新規登録</a>
			</div>

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" />
			</c:if>

			<div class="users">
				<c:forEach items="${ userBranchDepartments }" var="userBranchDepartment">
					<div class="user-infomation">
						<div class="account">アカウント  <c:out value="${ userBranchDepartment.account }" /></div>
						<div class="name">名前  <c:out value="${ userBranchDepartment.userName }" /></div>
						<div class="branch">支社  <c:out value="${ userBranchDepartment.branchName }" /></div>
						<div class="department">部署  <c:out value="${ userBranchDepartment.departmentName }" /></div>

						<c:if test="${ userBranchDepartment.isStopped == 0 }">
						<div class="is-stopeed">ステータス  アクティブ </div>
						</c:if>

						<c:if test="${ userBranchDepartment.isStopped == 1 }">
						<div class="is-stopeed">ステータス  停止中 </div>
						</c:if>

						<form action="setting" id="setting-button">
							<input name="id" value="${ userBranchDepartment.id }" type="hidden">
							<input type="submit" value="編集">
						</form>

						<c:if test="${ userBranchDepartment.id != loginUser.id }">
							<c:if test="${ userBranchDepartment.isStopped == 0 }">
								<form action="stop" method="post" id="stop-button" onclick="return stopConfirm()">
									<input type="hidden" name="id" value="${ userBranchDepartment.id }">
									<input type="hidden" name="is-stopped" value="1">
									<input type="submit" value="停止" id="stop">
								</form>
							</c:if>

							<c:if test="${ userBranchDepartment.isStopped == 1 }">
								<form action="stop" method="post" id="stop-button" onclick="return reviveConfirm()">
									<input type="hidden" name="id" value="${ userBranchDepartment.id }">
									<input type="hidden" name="is-stopped" value="0">
									<input type="submit" value="復活" id="stop">
								</form>
							</c:if>
						</c:if>
					</div>
				</c:forEach>
			</div>

			<div class="copyriight">Copyright(c) M.Amano</div>

		</div>
	</body>
</html>