<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<title>ユーザー登録</title>
	</head>

	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" />
			</c:if>

			<form action="signup" method="post">
				<div class="signup-user">
					<div>
						<label for="account">アカウント名</label>
						<input name="account" id="account" value=<c:out value="${ user.account }" />>(半角英数字6文字以上20文字以下)
					</div>

					<div>
						<label for="password">パスワード</label>
						<input name="password" type="password" id="password" >(記号を含む半角文字6文字以上20文字以下)
					</div>

					<div>
						<label for="password-to-confirm">パスワード(確認用)</label>
						<input name="password-to-confirm" type="password" id="password-to-confirm" >
					</div>

					<div>
						<label for="name">名称</label>
						<input name="name" id="name" value=<c:out value="${ user.name }" />>(10文字以下)
					</div>

					<div>
						<label for="barnch-id">支社</label>
						<select name="branch-id" id="branch-id">
							<c:forEach items="${ branches }" var="branch">
								<option value="${ branch.id }" <c:if test="${ branch.id == user.branchId }">selected</c:if>>
									${ branch.name }
								</option>
							</c:forEach>
						</select>
					</div>

					<div>
						<label for="department-id">部署</label>
						<select name="department-id">
							<c:forEach items="${ departments }" var="department">
								<option value="${ department.id }" <c:if test="${ department.id == user.departmentId }">selected</c:if>>
									${ department.name }
								</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<input type="submit" value="登録" >
			</form>

			<div class="footer">
				<a href="./">戻る</a>
			</div>

			<div class="copyright">Copyright(c) M.Amano</div>

		</div>
	</body>
</html>