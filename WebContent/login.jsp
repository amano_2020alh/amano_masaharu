<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="./css/style.css">

        <title>ログイン</title>
    </head>

    <body>
        <div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="error-messages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="request"/>
			</c:if>

			<c:if test="${ not empty errorLoginMessages }">
				<div class="error-login-messages">
					<ul>
						<c:forEach items="${ errorLoginMessages }" var="errorLoginMessage">
							<li><c:out value="${ errorLoginMessage }" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorLoginMessages" scope="session"/>
			</c:if>

            <form action="login" method="post">
                <label for="account">アカウント名</label>
                <input name="account" id="account" value="${ account }">

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" >

                <input type="submit" value="ログイン" >
            </form>

            <div class="copyright"> Copyright(c) M.Amano</div>

        </div>
    </body>
</html>